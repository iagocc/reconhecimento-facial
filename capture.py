import cv2
import numpy as np
import sys

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

video_capture = cv2.VideoCapture(0)
video_capture.set(3, 1280)
video_capture.set(4, 720)

list = []
save = False

saves = 0
while True:

    opt = cv2.waitKey(1)
    if opt == ord('q'):
        break
    elif opt == ord('c'):
      save = True
    elif opt == ord('s'):
      save = False

    # Capture frame-by-frame
    ret, frame = video_capture.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=1,
        minSize=(80, 80),
        flags=0
    )

    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
      if save and saves < 99:
        list.append((gray, x, y, w, h))
        saves = saves + 1
      cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    cv2.putText(frame, "Capturado " + str(saves) + " frames", (10,20), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,0,0), thickness=1, lineType=cv2.CV_AA)
    # Display the resulting frame
    cv2.imshow('Video', frame)

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()

sys.stdout.write("Salvando imagens ")
i = 0
for (img, x, y, w, h) in list:
  crop_img = img[y:y+h, x:x+w]
  sized_img = cv2.resize(crop_img, (350, 350)) 
  cv2.imwrite("images_test/face_"+str(i)+".png", sized_img)
  i = i+1
  sys.stdout.write('.')
print "\n"
