from __future__ import print_function

import cv2
import numpy as np
import sys
from datetime import datetime, time

from time import time
import logging
import matplotlib.pyplot as plt

from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.externals import joblib

import os
import numpy as np
from PIL import Image

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

video_capture = cv2.VideoCapture(1)
video_capture.set(3, 640)
video_capture.set(4, 480)

list = []
save = False

saves = 0
beginning = datetime.now()
final_frame = None
while True:
    now = datetime.now()
    secs = (now - beginning).seconds
    # Capture frame-by-frame
    ret, frame = video_capture.read()
    cv2.putText(frame, "Se passaram " + str(secs) + " segundos", (10,20), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,0,0), thickness=1, lineType=cv2.CV_AA)
    # Display the resulting frame
    cv2.imshow('Video', frame)
    if (secs >= 10):
        final_frame = frame
        break

gray = cv2.cvtColor(final_frame, cv2.COLOR_BGR2GRAY)
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=1,
    minSize=(80, 80),
    flags=0
)
for (x, y, w, h) in faces:
  list.append((gray, x, y, w, h))
  cv2.rectangle(final_frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imshow('Video', final_frame)

sized_img = None
for (img, x, y, w, h) in list:
  crop_img = img[y:y+h, x:x+w]
  sized_img = cv2.resize(crop_img, (350, 350))

cv2.putText(sized_img, "Face reconhecida", (0,0), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,0,0), thickness=1, lineType=cv2.CV_AA)
cv2.imshow('Imagem', sized_img)

sleep(5)

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()

#######################################################
X = np.array(sized_img).flatten()

pca = joblib.load("face_recognition_pca.pkl")
X_pca = pca.transform(X)

clf = joblib.load("face_recognition_svm.pkl")
y_pred = clf.predict(X_pca)

print(y_pred)