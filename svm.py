from __future__ import print_function

from time import time
import logging
import matplotlib.pyplot as plt

from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import normalize

import os
import numpy as np
from PIL import Image

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

###############################################################################

print("Loading images:")
t0 = time()

images = np.empty([1,122500])
n = 0;
dic = {}
for filename in os.listdir(os.getcwd()+"/images"):
    if "face" in filename :
        img = np.array(Image.open("images/"+filename)) # image to numpy array
        img = img.flatten() # matrix to array
        images = np.vstack([images, img])
        dic[n] = int(filename[4])
        n += 1

images = np.delete(images, (0), axis=0) # remove first row(trash)
X = images
y = np.zeros((X.shape[0],))

# set class of image
for k,v in dic.iteritems():
    y[k] = v


n_features = X.shape[1]
n_classes = 3
n_samples = X.shape[0]
h, w = X.shape

print("Total dataset size:")
print("n_samples: %d" % n_samples)
print("n_features: %d" % n_features)
print("n_classes: %d" % n_classes)

print("w: %d" % w)
print("h: %d" % h)

print("done in %0.3fs" % (time() - t0))

###############################################################################
# Divisao
X = normalize(X)

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25)

###############################################################################
# PCA
print("Executing PCA:")
t0 = time()

n_components = 0.999

pca = PCA(n_components=n_components).fit(X_train)
X_train_pca = pca.transform(X_train)
X_test_pca = pca.transform(X_test)

print("done in %0.3fs" % (time() - t0))

print(X_train_pca.shape)

###############################################################################
# SVM

print("Executing SVM:")
t0 = time()
# param_grid = {'C': [0.1, 1.0, 10,], 'kernel': ['linear'] }
# clf = GridSearchCV(
#         SVC(),
#         param_grid
#     )
clf = SVC(kernel='linear')
clf = clf.fit(X_train_pca, y_train)
print("done in %0.3fs" % (time() - t0))

###############################################################################
# Teste

print("Predicting people's names on the test set")
t0 = time()
y_pred = clf.predict(X_test_pca)
print("done in %0.3fs" % (time() - t0))

print(classification_report(y_test, y_pred, target_names=["Iago", "Iracema", "Ismael"]))
print(confusion_matrix(y_test, y_pred))

###############################################################################
# Save

svmFileName = "dump/face_recognition_svm.pkl"
pcaFileName = "dump/face_recognition_pca.pkl"
joblib.dump(clf, svmFileName)
joblib.dump(pca, pcaFileName)

# del images
# del X
# del y_pred
# del y

# n = 0;
# dic = {}
# images = np.empty([1,122500])
# for filename in os.listdir(os.getcwd()+"/images_test"):
#     if "face" in filename :
#         img = np.array(Image.open("images_test/"+filename)) # image to numpy matrix
#         img = img.flatten() # matrix to array
#         images = np.vstack([images, img])
#         dic[n] = int(filename[4])
#         n += 1

# images = np.delete(images, (0), axis=0)

# X = images
# X = normalize(X)

# y = np.zeros((X.shape[0],))

# # set class of image
# for k,v in dic.iteritems():
#     y[k] = v

# ###############################################################################
# # PCA

# print("Executing PCA:")
# X_pca = pca.transform(X)

# print("Executing SVM:")
# y_pred = clf.predict(X_pca)

# print("Result:")
# print(classification_report(y, y_pred, target_names=["Iago", "Iracema", "Ismael"]))
# print(confusion_matrix(y, y_pred))
