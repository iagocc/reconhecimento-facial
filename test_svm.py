from __future__ import print_function

from time import time
import logging
import matplotlib.pyplot as plt

from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.preprocessing import normalize

import os
import numpy as np
from PIL import Image

images = np.empty([1,122500])
for filename in os.listdir(os.getcwd()+"/images_test"):
    if "face" in filename :
        img = np.array(Image.open("images_test/"+filename)) # image to numpy array
        img = img.flatten() # matrix to array
        images = np.vstack([images, img])

images = np.delete(images, (0), axis=0)
X = images
X = normalize(X)

###############################################################################
# PCA
print("Executing PCA:")

pca = joblib.load("dump/face_recognition_pca.pkl")
X_pca = pca.transform(X)

clf = joblib.load("dump/face_recognition_svm.pkl")

y_pred = clf.predict(X_pca)

print(classification_report(np.ones(y_pred.size), y_pred, target_names=["Iago", "Iracema", "Ismael"]))

print(confusion_matrix(np.ones(y_pred.size), y_pred))

print(y_pred)

print("Number of 1's: %d" % y_pred.tolist().count(1))
print("Number of 2's: %d" % y_pred.tolist().count(2))
print("Number of 3's: %d" % y_pred.tolist().count(3))
print("Total: %d" % len(y_pred.tolist()))
print("Acc of 1's: %f" % (float(y_pred.tolist().count(1))/len(y_pred.tolist())))